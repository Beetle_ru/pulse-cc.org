console.log("runLine current directory -> " + process.cwd());
var fs = require("fs");
exec  = require('child_process').exec;
var path = {
	dir : "public/content/rl/",
	rl1  : "rl1.html",
	rl2  : "rl2.html",
	rl3  : "rl3.html"
};

module.exports = function (interval) {
	global.runLine = {
		rl1 : "",
		rl2 : "",
		rl3 : ""
	}
	execRLParser();
	setInterval(execRLParser,interval);		
}

function ubdateRL() {
	//console.log("ok");
	fs.readFile(path.dir + path.rl1,function(error, content) {
		if(error) {
			console.log(error);
		}
		else {
			global.runLine.rl1 = content;
		}
	});
	fs.readFile(path.dir + path.rl2,function(error, content) {
		if(error) {
			console.log(error);
		}
		else {
			global.runLine.rl2 = content;
		}
	});
	fs.readFile(path.dir + path.rl3,function(error, content) {
		if(error) {
			console.log(error);
		}
		else {
			global.runLine.rl3 = content;
		}
	});
}

function execRLParser() {
	exec('rssParser/rssParser.sh', function (error, stdout, stderr) {
		if(error) {
			console.log("rssParser.sh return error");
			console.log(error);
		}
		else {
			ubdateRL(); // читаем файлы и обновляем переменные
		}
	});
}
