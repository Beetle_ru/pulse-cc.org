var url = require("url");
var fs = require("fs");

module.exports = function (req, res) {
	var parameters = url.parse(req.url, true);
	var path = parameters.query.content;
	if (typeof(path) == 'undefined') {
		path = 'main'
	}
	console.log(req.connection.remoteAddress + " (ajax) ==>> " + path);
	fs.readFile('public/content/' + path + '.html',function(error, content) {
		if(error) {
			console.log('------------------------------------------------------------------');
			console.log(req.connection.remoteAddress);
			console.log(error);
			console.log('------------------------------------------------------------------');
			content = '<br><br><h1>Запрашиваемая страница не доступна</h1><br>' + error;
		}
		res.render('ajax',{ title: 'pulse-cc.org', content: content })
	});
	//res.render('main',{ title: 'pulse-cc.org' })
};
